/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mongkol.projectmid;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author DIY
 */
public class ProducService {
    static ArrayList<Produc> addProduct = new ArrayList<>();
    static{
        addProduct = new ArrayList<>();
        
        addProduct.add(new Produc("1","Rice","easy",15.00,1));
        addProduct.add(new Produc("2","Sausage","Ceepee",35.00,1));
        addProduct.add(new Produc("3","Snack","ArhanYodkhun",10.5,2));
        load();
    }
    //Create(c)
    public static boolean addProduc(Produc produc){
        addProduct.add(produc);
        save();
        return true;
    }
    //Delete(D)
     public static boolean delProduc(Produc produc){
        addProduct.remove(produc);
        save();
        return true;
    }
     
      public static boolean delProduc(int index){
        addProduct.remove(index);
        save();
        return true;
    }
      //Rrad(R)
     public static ArrayList<Produc> getProduc(){
         return addProduct;
     } 
     public static Produc getProduc(int index){
         return addProduct.get(index);
     }
     //Update(U)
     public static boolean updateProduc(int index,Produc produc){
         addProduct.set(index, produc);
         save();
         return true;
     }
     public static void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("Tar.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(addProduct);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProducService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProducService.class.getName()).log(Level.SEVERE, null, ex);
        }
 
     }
     public static void load(){
         
     File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("Tar.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            addProduct = (ArrayList<Produc>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProducService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProducService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProducService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}


   // static Produc getProduc(int index) {
   //     throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    

